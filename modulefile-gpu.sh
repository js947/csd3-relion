#!/bin/bash
set -euo pipefail

PREFIX=$SOFTWARE/relion/gpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
MODULEFILE=$MODULEFILES/relion-gpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID 

mkdir -p $(dirname $MODULEFILE)
cat >$MODULEFILE <<EOF
#%Module -*- tcl -*-
module-whatis "adds Relion $CI_COMMIT_REF_NAME (built for the csd3 pascal partition) to your environment"

puts stderr "When submitting jobs, please set:"
puts stderr "\tQueue name:\t\tpascal"
puts stderr "\tQueue submit command:\tsbatch"
puts stderr "Also please remember to set the correct project to charge."

prereq fltk-1.3.3-gcc-4.8.5-rldrmzh
prereq cuda/10.1

prepend-path      PATH                  $PREFIX/bin
prepend-path      LD_LIBRARY_PATH       $PREFIX/lib
prepend-path      LIBRARY_PATH          $PREFIX/lib
setenv            RELION_QSUB_TEMPLATE       $PREFIX/slurm.sh
setenv            RELION_QSUB_EXTRA1         "Project to charge:"
setenv            RELION_QSUB_EXTRA2         "Walltime:"
setenv            RELION_QSUB_EXTRA1_DEFAULT CHANGEME
setenv            RELION_QSUB_EXTRA2_DEFAULT 2:00:0
setenv            RELION_QSUB_EXTRA_COUNT 2
EOF
