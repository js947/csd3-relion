#!/bin/bash
set -euo pipefail

cat >$SOFTWARE/relion/cpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID/slurm.sh <<EOF
#!/bin/bash
#SBATCH -J relion
#SBATCH -A XXXextra1XXX
#SBATCH -p XXXqueueXXX
#SBATCH -N XXXnodesXXX
#SBATCH -n XXXmpinodesXXX
#SBATCH --cpus-per-task=XXXthreadsXXX
#SBATCH --time=XXXextra2XXX
#SBATCH --mail-type=FAIL

. /etc/profile.d/modules.sh
module purge
module load rhel7/default-gpu fltk-1.3.3-gcc-4.8.5-rldrmzh
module load relion/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID

export OMP_NUM_THREADS=XXXthreadsXXX
mpirun XXXcommandXXX
EOF