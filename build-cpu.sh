#!/bin/bash
set -euo pipefail

SRC=$(realpath ./relion)
BUILD=$(mktemp -d)
PREFIX=$SOFTWARE/relion/cpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
trap "rm -rf $BUILD" EXIT

cd $BUILD

module load cmake
module load fltk-1.3.3-gcc-4.8.5-rldrmzh

CFLAGS="-O3 -ip -xCORE-AVX2 -axCORE-AVX512,MIC-AVX512 -restrict"

cmake $SRC -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc \
  -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_CXX_FLAGS="$CFLAGS" \
  -DALTCPU=ON -DCUDA=OFF -DMKLFFT=ON \
  -DMPI_CXX_INCLUDE_DIRS=/usr/local/Cluster-Apps/intel/2017.4/compilers_and_libraries_2017.4.196/linux/mpi/intel64/include
make -j24
make install
