#!/bin/bash
set -euo pipefail

PREFIX=$SOFTWARE/relion/cpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
MODULEFILE=$MODULEFILES/relion/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID 

mkdir -p $(dirname $MODULEFILE)
cat >$MODULEFILE <<EOF
#%Module -*- tcl -*-
module-whatis "adds Relion $CI_COMMIT_REF_NAME (built for the csd3 skylake & knl partitions) to your environment"

puts stderr "When submitting jobs, please set:"
puts stderr "\tQueue name:\t\tskylake (or knl)"
puts stderr "\tQueue submit command:\tsbatch"
puts stderr "Also please remember to set the correct project to charge."

prereq intel/bundles/complib/2017.4
prereq fltk-1.3.3-gcc-4.8.5-rldrmzh

prepend-path      PATH                  $PREFIX/bin
prepend-path      LD_LIBRARY_PATH       $PREFIX/lib
prepend-path      LIBRARY_PATH          $PREFIX/lib
setenv            RELION_QSUB_TEMPLATE       $PREFIX/slurm.sh
setenv            RELION_QSUB_EXTRA1         "Project to charge:"
setenv            RELION_QSUB_EXTRA2         "Walltime:"
setenv            RELION_QSUB_EXTRA1_DEFAULT CHANGEME
setenv            RELION_QSUB_EXTRA2_DEFAULT 2:00:0
setenv            RELION_QSUB_EXTRA_COUNT 2
EOF