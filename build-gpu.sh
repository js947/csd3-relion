#!/bin/bash
set -euo pipefail

ARCH=sm_60

SRC=$(realpath ./relion)
BUILD=$(mktemp -d)
PREFIX=$SOFTWARE/relion/gpu/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
trap "rm -rf $BUILD" EXIT

cd $BUILD

module load cmake
module load fltk-1.3.3-gcc-4.8.5-rldrmzh
module swap cuda cuda/10.1

CFLAGS="-O3 -march=native -fstrict-aliasing"

cmake $SRC -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ \
  -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_CXX_FLAGS="$CFLAGS" -DCUDA_ARCH=60 \
  -DALTCPU=OFF -DCUDA=ON
  #-DMPI_INCLUDE_PATH=$(mpicc --showme:incdirs)
make -j24
make install
